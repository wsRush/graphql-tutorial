import db from './_db.js'

export const resolvers = {
    Query: {
        games() {
            return db.games
        },
        authors() {
            return db.authors
        },
        reviews() {
            return db.reviews
        },
        // query variables, see it's section in readme to querify
        review(_, args) {
            return db.reviews.find((review) => review.id === args.id)
        },
        game(_, args) {
            return db.games.find((game) => game.id === args.id)
        },
        author(_, args) {
            return db.authors.find((author) => author.id === args.id)
        },
    },
    // related data, when you request related data for example reviews of a game, the resolver game above will search for reviews in Game object resolvers down
    Game: {
        reviews(parent) {
            return db.reviews.filter(review => review.game_id === parent.id)
        }
    },
    Author: {
        reviews(parent) {
            return db.reviews.filter(review => review.author_id === parent.id)
        }
    },
    Review: {
        game(parent) {
            return db.games.find(game => game.id === parent.game_id)
        },
        author(parent) {
            return db.authors.find(author => author.id === parent.author_id)
        }
    },
    // muattions: see Readme
    Mutation: {
        deleteGame(_, args) {
            db.games = db.games.filter(game => game.id !== args.id)
            return db.games
        },
        addGame(_, args) {
            let game = {
                ...args.game,
                id: Math.floor(Math.random() * 10000).toString()
            }

            db.games.push(game)
            return game
        },
        updateGame(_, args) {
            db.games = db.games.map(game => {
                if (game.id === args.id) return {...game, ...args.edits}

                return game
            })

            return db.games.find(game => game.id === args.id)
        }
    }
}
