import { ApolloServer } from "@apollo/server";
import { startStandaloneServer } from '@apollo/server/standalone'

import { typeDefs } from "./schema.js";
import { resolvers } from "./resolvers.js";

// server setup
const server = new ApolloServer({
    // typeDefs: description of our data and the relationship they have with other data types
    typeDefs,
    // resolver: bunch of resolver functions that determine how we respond to queries for diffrent data on the graph
    resolvers
})

const { url } = await startStandaloneServer(server, {
    listen: { port: 4000 }
})

console.log('Server ready at port', 4000)